package br.com.estacionamento.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.estacionamento.DAO.FuncionarioDAO;
import br.com.estacionamento.entidade.Funcionario;
import br.com.estacionamento.util.Mensagem;


@ManagedBean(name="FuncionarioController")
public class FuncionarioController {

	private Funcionario funcionario;
	
	@EJB
	private FuncionarioDAO funcionarioDAO;
	
	
	public FuncionarioController(){
		this.funcionario = new Funcionario();
	}
	
	public void salvar(){
		String erro = funcionarioDAO.salvar(funcionario);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.funcionario = new Funcionario(); //Limpar os campos
		}	
	}
	
	public void excluir(Funcionario funcionario){
		String erro = funcionarioDAO.excluir(funcionario.getIdPessoa());				
	
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Exclu�do com sucesso.");
		}
	}
	
	public void editar(Funcionario funcionarioEditado){
		this.funcionario = funcionarioEditado;
	}
	
	public List<Funcionario> consultar(){
		return funcionarioDAO.todos();
	}
	

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}
	
	
}
