package br.com.estacionamento.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.estacionamento.DAO.VeiculoDAO;
import br.com.estacionamento.entidade.Veiculo;
import br.com.estacionamento.util.Mensagem;

@ManagedBean(name="VeiculoController")
public class VeiculoController {

	private Veiculo veiculo;
	
	@EJB
	private VeiculoDAO veiculoDAO;
	
	public VeiculoController(){
		this.veiculo = new Veiculo();
	}
	
	public void salvar(){
		String erro = veiculoDAO.salvar(veiculo);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.veiculo = new Veiculo(); //Limpar os campos
		}	
	}
	
	public void excluir(Veiculo pessoa){
		String erro = veiculoDAO.excluir(pessoa.getIdVeiculo());				
	
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Exclu�do com sucesso.");
		}
	}
	
	public void editar(Veiculo veiculoEditado){
		this.veiculo = veiculoEditado;
	}
	
	public List<Veiculo> consultar(){
		return veiculoDAO.todos();
	}
	

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
}
