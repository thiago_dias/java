package br.com.estacionamento.controller;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.estacionamento.DAO.Usuario_LoginDAO;
import br.com.estacionamento.entidade.Usuario_Login;
import br.com.estacionamento.util.Mensagem;



@ManagedBean
public class UsuarioController {
	private Usuario_Login usuario_login;

	@EJB
	private Usuario_LoginDAO usuario_LoginDAO;
	
	public UsuarioController(){
		this.usuario_login = new Usuario_Login();
	}

	public String consultar(){
		
		 String nome = usuario_LoginDAO.todos(usuario_login.getNome(),usuario_login.getSenha());
		
		if(nome == null){
			Mensagem.erro("Email ou Senha incorreto");
			this.usuario_login = new Usuario_Login();
			return null;
		}
		else{	
			return "index.xhtml";
		}
	}
	
	public Usuario_Login getUsuario_login() {
		return usuario_login;
	}

	public void setUsuario_login(Usuario_Login usuario_login) {
		this.usuario_login = usuario_login;
	}

}
