package br.com.estacionamento.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.estacionamento.DAO.PessoaDAO;
import br.com.estacionamento.entidade.Pessoa;
import br.com.estacionamento.util.Mensagem;


@ManagedBean(name="PessoaController")
public class PessoaController {

	private Pessoa pessoa;
	
	@EJB
	private PessoaDAO pessoaDAO;
	
	
	public PessoaController(){
		this.pessoa = new Pessoa();
	}
	
	public void salvar(){
		String erro = pessoaDAO.salvar(pessoa);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.pessoa = new Pessoa(); //Limpar os campos
		}	
	}
	
	public void excluir(Pessoa pessoa){
		String erro = pessoaDAO.excluir(pessoa.getIdPessoa());				
	
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Exclu�do com sucesso.");
		}
	}
	
	public void editar(Pessoa pessoaEditado){
		this.pessoa = pessoaEditado;
	}
	
	public List<Pessoa> consultar(){
		return pessoaDAO.todos();
	}
	

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	
	
}
