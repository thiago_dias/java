package br.com.estacionamento.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.estacionamento.DAO.TipoVeiculoDAO;
import br.com.estacionamento.entidade.TipoVeiculo;
import br.com.estacionamento.util.Mensagem;


@ManagedBean(name="TipoveiculoController")
public class TipoveiculoController {

	public TipoVeiculo getTipoveiculo() {
		return tipoveiculo;
	}

	public void setTipoveiculo(TipoVeiculo tipoveiculo) {
		this.tipoveiculo = tipoveiculo;
	}

	public TipoVeiculoDAO getTipoveiculoDAO() {
		return tipoveiculoDAO;
	}

	public void setTipoveiculoDAO(TipoVeiculoDAO tipoveiculoDAO) {
		this.tipoveiculoDAO = tipoveiculoDAO;
	}

	private TipoVeiculo tipoveiculo;
	
	@EJB
	private TipoVeiculoDAO tipoveiculoDAO;
	
	
	public TipoveiculoController(){
		this.tipoveiculo = new TipoVeiculo();
	}
	
	public void salvar(){
		String erro = tipoveiculoDAO.salvar(tipoveiculo);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.tipoveiculo = new TipoVeiculo(); //Limpar os campos
		}	
	}
	
	public void excluir(TipoVeiculo tipoveiculo){
		String erro = tipoveiculoDAO.excluir(tipoveiculo.getIdTipoVeiculo());
	
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Exclu�do com sucesso.");
		}
	}
	
	public void editar(TipoVeiculo tipoveiculoEditado){
		this.tipoveiculo = tipoveiculoEditado;
	}
	
	public List<TipoVeiculo> consultar(){
		return tipoveiculoDAO.todos();
	}
	
	public TipoVeiculo getTipoVeiculo() {
		return tipoveiculo;
	}

	public void settipoveiculo(TipoVeiculo tipoveiculo) {
		this.tipoveiculo = tipoveiculo;
	}
	
	
}
