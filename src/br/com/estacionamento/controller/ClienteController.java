package br.com.estacionamento.controller;

import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import br.com.estacionamento.DAO.ClienteDAO;
import br.com.estacionamento.entidade.Cliente;
import br.com.estacionamento.util.Mensagem;

@ManagedBean(name="ClienteController")
public class ClienteController {
private Cliente cliente;
	
	@EJB
	private ClienteDAO clienteDAO;
	
	
	public ClienteController(){
		this.cliente = new Cliente();
	}
	
	public void salvar(){
		String erro = clienteDAO.salvar(cliente);
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Salvo com sucesso.");
			this.cliente = new Cliente(); //Limpar os campos
		}	
	}
	
	public void excluir(Cliente cliente){
		String erro = clienteDAO.excluir(cliente.getIdPessoa());				
	
		if(erro != null){
			Mensagem.erro("Ocorreu um erro: "+erro);
		}else{
			Mensagem.sucesso("Exclu�do com sucesso.");
		}
	}
	
	public void editar(Cliente clienteEditado){
		this.cliente = clienteEditado;
	}
	
	public List<Cliente> consultar(){
		return clienteDAO.todos();
	}
	

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	

}
