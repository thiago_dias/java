package br.com.estacionamento.entidade;

import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;



@Entity
@PrimaryKeyJoinColumn(name ="idPessoa")
@DiscriminatorValue("C")
public class Cliente extends Pessoa {
	private static final long serialVersionUID = 1L;
	@OneToMany(mappedBy = "cliente")
	private List<Veiculo> veiculo;


	public List<Veiculo> getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(List<Veiculo> veiculo) {
		this.veiculo = veiculo;
	}
	

}
