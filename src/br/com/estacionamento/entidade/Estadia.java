package br.com.estacionamento.entidade;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


@Entity
public class Estadia {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idEstadia;
	
	@NotNull(message="Digita a Hora de Entrada do Veiculo")
	@Temporal(TemporalType.TIMESTAMP)
	private Date hentrada;
	@Temporal(TemporalType.TIMESTAMP)
	private Date hsaida;
	@NotNull(message="Digita a Data de Entrada do Veiculo")

	@ManyToOne
	@JoinColumn(name = "idVeiculo")
	private Veiculo veiculoestadia;

	
	public Integer getIdEstadia() {
		return idEstadia;
	}
	public void setIdEstadia(Integer idEstadia) {
		this.idEstadia = idEstadia;
	}
	public Date getHentrada() {
		return hentrada;
	}
	public void setHentrada(Date hentrada) {
		this.hentrada = hentrada;
	}
	public Date getHsaida() {
		return hsaida;
	}
	public void setHsaida(Date hsaida) {
		this.hsaida = hsaida;
	}
	public Veiculo getVeiculoestadia() {
		return veiculoestadia;
	}
	public void setVeiculoestadia(Veiculo veiculoestadia) {
		this.veiculoestadia = veiculoestadia;
	}

}
