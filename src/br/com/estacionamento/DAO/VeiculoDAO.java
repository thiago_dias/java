package br.com.estacionamento.DAO;

import javax.ejb.Stateless;

import br.com.estacionamento.entidade.Veiculo;

@Stateless
public class VeiculoDAO extends GenericDAO<Veiculo> {
	
	public VeiculoDAO(){
		super(Veiculo.class);
	}
}
