package br.com.estacionamento.DAO;

import javax.ejb.Stateless;

import br.com.estacionamento.entidade.TipoVeiculo;

@Stateless
public class TipoVeiculoDAO extends GenericDAO<TipoVeiculo> {
	
	public TipoVeiculoDAO(){
		super(TipoVeiculo.class);
	}
}

