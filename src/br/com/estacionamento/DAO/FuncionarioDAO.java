package br.com.estacionamento.DAO;

import javax.ejb.Stateless;

import br.com.estacionamento.entidade.Funcionario;

@Stateless
public class FuncionarioDAO extends GenericDAO<Funcionario> {
	
	public FuncionarioDAO(){
		super(Funcionario.class);
	}
}
