package br.com.estacionamento.DAO;

import javax.ejb.Stateless;

import br.com.estacionamento.entidade.Pessoa;


@Stateless
public class PessoaDAO extends GenericDAO<Pessoa> {
	
	public PessoaDAO(){
		super(Pessoa.class);
	}
}
