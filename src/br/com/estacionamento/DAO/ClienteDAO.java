package br.com.estacionamento.DAO;

import javax.ejb.Stateless;

import br.com.estacionamento.entidade.Cliente;


@Stateless
public class ClienteDAO extends GenericDAO<Cliente> {
	
	public ClienteDAO(){
		super(Cliente.class);
	}
}

