package br.com.estacionamento.DAO;

import javax.ejb.Stateless;

import br.com.estacionamento.entidade.Estadia;

@Stateless
public class EstadiaDAO extends GenericDAO<Estadia> {
	
	public EstadiaDAO(){
		super(Estadia.class);
	}
}
