package br.com.estacionamento.exportar;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedProperty;

import br.com.estacionamento.DAO.FuncionarioDAO;
import br.com.estacionamento.entidade.Funcionario;


public class Export implements Serializable {

private List<Funcionario> funcionario;
	
	@ManagedProperty("#{FuncionarioDAO}")
	private FuncionarioDAO service;
	
	 @PostConstruct
	 public void init() {
		 funcionario = service.todos();
	 }
	 public List<Funcionario> getfuncionarios() {
	        return funcionario;
	 }

	 public void setService(FuncionarioDAO service) {
	        this.service = service;
	 }
}
